package com.lardi.phoneBook;


import com.lardi.phoneBook.phoneBook.dao.UserDao;
import com.lardi.phoneBook.phoneBook.entity.User;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class UserTest {

    @Autowired
    private UserDao userDao;

    @Test
    public void createTestDb() {
        User user = new User();
        user.setLogin("login");
        user.setPassword("password");
        user.setName("name");
        user.setId(userDao.create(user));
        User user1 = userDao.getById(user.getId());
        assert (user.equals(user1));


    }
}
