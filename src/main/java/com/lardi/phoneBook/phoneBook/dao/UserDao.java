package com.lardi.phoneBook.phoneBook.dao;


import com.lardi.phoneBook.phoneBook.entity.User;

public interface UserDao {

    int create(User user);

    User getById(int id);


}
